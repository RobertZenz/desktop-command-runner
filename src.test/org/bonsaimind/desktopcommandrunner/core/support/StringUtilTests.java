
package org.bonsaimind.desktopcommandrunner.core.support;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class StringUtilTests {
	@Test
	public void testConvertCommandToCommandLine() {
		assertListEquals(
				Arrays.asList(),
				StringUtil.convertToCommandLine(""));
		assertListEquals(
				Arrays.asList("abc"),
				StringUtil.convertToCommandLine("abc"));
		assertListEquals(
				Arrays.asList("abc", "123", "xyz"),
				StringUtil.convertToCommandLine("abc 123 xyz"));
		assertListEquals(
				Arrays.asList("abc", "123", "xyz"),
				StringUtil.convertToCommandLine("abc\t  123  \t xyz"));
		assertListEquals(
				Arrays.asList("abc", "123  \t xyz"),
				StringUtil.convertToCommandLine("abc\t  \"123  \t xyz\""));
		assertListEquals(
				Arrays.asList("abc", "123  \t xyz"),
				StringUtil.convertToCommandLine("abc\t  '123  \t xyz'"));
		assertListEquals(
				Arrays.asList("abc", "123  \"\t xyz"),
				StringUtil.convertToCommandLine("abc\t  '123  \"\t xyz'"));
		
		assertListEquals(
				Arrays.asList("-xyz", "--long", "--word=value", "--longword=1 2 3 4"),
				StringUtil.convertToCommandLine("-xyz --long --word=value --longword=\"1 2 3 4\""));
		assertListEquals(
				Arrays.asList("-xyz", "--long", "--word=value", "--longword=1 2 3 4"),
				StringUtil.convertToCommandLine("-xyz --long --word=value --longword='1 2 3 4'"));
		assertListEquals(
				Arrays.asList("-xyz", "--long", "--word=value", "--longword=1\n2\n3\n4"),
				StringUtil.convertToCommandLine("-xyz\n--long\n--word=value\n--longword='1\n2\n3\n4'"));
	}
	
	protected <LIST_TYPE> void assertListEquals(List<LIST_TYPE> expectedList, List<LIST_TYPE> actualList) {
		if (expectedList.size() != actualList.size()) {
			Assertions.fail(String.format("Expected List with <%d> elements but got <%d> elements.",
					Integer.valueOf(expectedList.size()),
					Integer.valueOf(actualList.size())));
		}
		
		for (int index = 0; index < actualList.size(); index++) {
			LIST_TYPE expectedItem = expectedList.get(index);
			LIST_TYPE actualItem = actualList.get(index);
			
			if (!Objects.equals(expectedItem, actualItem)) {
				Assertions.fail(String.format("Unexpected item at index <%d>, expected <%s> but got <%s>.",
						Integer.valueOf(index),
						expectedItem,
						actualItem));
			}
		}
	}
}
