/*
 *    Copyright 2024 Robert 'Bobby' Zenz
 * 
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 * 
 *        http://www.apache.org/licenses/LICENSE-2.0
 * 
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.bonsaimind.desktopcommandrunner;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

public final class Help {
	private Help() {
	}
	
	public static final String getHelpText() throws IOException {
		try (InputStream helpTextFileStream = Help.class.getResourceAsStream("help.text")) {
			try (ByteArrayOutputStream helpTextByteStream = new ByteArrayOutputStream()) {
				byte[] buffer = new byte[16 * 1024];
				int read = 0;
				
				while ((read = helpTextFileStream.read(buffer)) >= 0) {
					if (read > 0) {
						helpTextByteStream.write(buffer, 0, read);
					}
				}
				
				return new String(helpTextByteStream.toByteArray(), StandardCharsets.UTF_8);
			}
		}
	}
}
