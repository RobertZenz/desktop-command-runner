/*
 *    Copyright 2024 Robert 'Bobby' Zenz
 * 
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 * 
 *        http://www.apache.org/licenses/LICENSE-2.0
 * 
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.bonsaimind.desktopcommandrunner;

import java.awt.GraphicsEnvironment;
import java.nio.file.Path;

import org.bonsaimind.desktopcommandrunner.core.CommandStack;
import org.bonsaimind.desktopcommandrunner.core.parsers.CommandStackParser;
import org.bonsaimind.desktopcommandrunner.core.readers.FileReader;
import org.bonsaimind.desktopcommandrunner.core.readers.StreamReader;
import org.bonsaimind.desktopcommandrunner.core.support.ExceptionUtil;
import org.bonsaimind.desktopcommandrunner.core.support.logging.Logger;
import org.bonsaimind.desktopcommandrunner.ui.Ui;

public final class Main {
	private static final Logger LOGGER = new Logger(Main.class);
	
	private Main() {
	}
	
	public static void main(String[] args) throws Exception {
		Parameters parameters = new Parameters(args);
		
		if (parameters.getHelp()) {
			System.out.println(Help.getHelpText());
			
			System.exit(0);
		}
		
		Logger.setGlobalClassnameTagLevel(parameters.getClassnameTagLevel());
		Logger.setGlobalLevel(parameters.getLogLevel());
		Logger.setGlobalLevelTagLevel(parameters.getLevelTagLevel());
		
		Ui ui = null;
		String uiClassName = parameters.getUi();
		
		if (uiClassName == null) {
			if (!GraphicsEnvironment.isHeadless()) {
				uiClassName = "Swing";
			} else {
				uiClassName = "CommandLine";
			}
		}
		
		if (!uiClassName.contains(".")) {
			uiClassName = Main.class.getPackage().getName()
					+ ".ui."
					+ uiClassName.toLowerCase()
					+ "."
					+ uiClassName
					+ "Ui";
		}
		
		try {
			Class<?> uiClass = Main.class.getClassLoader().loadClass(uiClassName);
			Object loadedUiInstance = uiClass.getConstructor().newInstance();
			
			if (!(loadedUiInstance instanceof Ui)) {
				LOGGER.error("Loaded UI class <%s> is not a <Ui> class.",
						loadedUiInstance.getClass().getName());
				
				System.exit(1);
			}
			
			ui = (Ui)loadedUiInstance;
		} catch (ReflectiveOperationException e) {
			LOGGER.error("Failed to load UI class <%s>: %s",
					uiClassName,
					ExceptionUtil.getRootCause(e).getMessage(),
					e);
			
			System.exit(1);
		}
		
		CommandStack commandStack = new CommandStack();
		
		CommandStackParser commandStackParser = new CommandStackParser();
		FileReader fileReader = new FileReader();
		StreamReader streamReader = new StreamReader();
		
		for (Path commandStackFile : parameters.getFilesToLoad()) {
			if (commandStackFile.toString().equals("-")) {
				commandStack.addCommandStack(streamReader.readCommandStack(System.in, commandStackParser));
			} else {
				commandStack.addCommandStack(fileReader.readCommandStack(commandStackFile, commandStackParser));
			}
		}
		
		ui.setCommandStack(commandStack);
		
		if (parameters.getExecute()) {
			ui.execute();
		}
	}
}
