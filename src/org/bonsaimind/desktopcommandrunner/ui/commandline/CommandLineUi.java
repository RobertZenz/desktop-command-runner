/*
 *    Copyright 2024 Robert 'Bobby' Zenz
 * 
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 * 
 *        http://www.apache.org/licenses/LICENSE-2.0
 * 
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.bonsaimind.desktopcommandrunner.ui.commandline;

import org.bonsaimind.desktopcommandrunner.core.Command;
import org.bonsaimind.desktopcommandrunner.core.CommandStack;
import org.bonsaimind.desktopcommandrunner.core.ExecutionProgressListener;
import org.bonsaimind.desktopcommandrunner.core.Executor;
import org.bonsaimind.desktopcommandrunner.core.process.OutputLine;
import org.bonsaimind.desktopcommandrunner.core.process.OutputLineFormatter;
import org.bonsaimind.desktopcommandrunner.core.support.ExceptionUtil;
import org.bonsaimind.desktopcommandrunner.core.support.StringUtil;
import org.bonsaimind.desktopcommandrunner.ui.Ui;

public class CommandLineUi implements ExecutionProgressListener, Ui {
	protected CommandStack commandStack = null;
	
	public CommandLineUi() {
		super();
	}
	
	@Override
	public void commandExecutionBegins(Command command) {
		String name = command.getName();
		
		if (command.isFailureAbortsExecution()) {
			name = "(!) " + name;
		}
		
		System.out.println(StringUtil.padLeftRight(name, "=", 80));
	}
	
	@Override
	public void commandExecutionFinished(Command command) {
		if (command.getExecutionResult().wasSuccessful()) {
			System.out.format("EXIT: 0\n");
		} else if (command.getExecutionResult().getExitCode() != null) {
			System.out.format("EXIT: %d\n",
					command.getExecutionResult().getExitCode());
		} else if (command.getExecutionResult().getThrowable() != null) {
			Throwable rootCause = ExceptionUtil.getRootCause(command.getExecutionResult().getThrowable());
			
			System.out.format("EXCEPTION: %s: %s\n",
					rootCause.getClass().getSimpleName(),
					rootCause.getMessage());
			command.getExecutionResult().getThrowable().printStackTrace(System.out);
		}
	}
	
	@Override
	public void commandExecutionNewOutput(Command command, OutputLine outputLine) {
		System.out.print(OutputLineFormatter.DEFAULT.format(outputLine));
	}
	
	@Override
	public void commandStackExecutionBegins(CommandStack commandStack) {
		// NOOP
	}
	
	@Override
	public void commandStackExecutionFinished(CommandStack commandStack) {
		System.out.println();
		
		for (Command command : commandStack.getCommands()) {
			String abortMarker = "";
			
			if (command.isFailureAbortsExecution()) {
				abortMarker = "(!) ";
			}
			
			if (command.getExecutionResult() != null) {
				if (command.getExecutionResult().getThrowable() != null) {
					System.out.format("<[ERR] %s%s\n",
							abortMarker,
							command.getName());
				} else {
					System.out.format("<[%3d] %s%s\n",
							command.getExecutionResult().getExitCode(),
							abortMarker,
							command.getName());
				}
			} else {
				System.out.format("<[   ] %s%s\n",
						abortMarker,
						command.getName());
			}
		}
	}
	
	@Override
	public void execute() {
		Executor executor = new Executor();
		
		executor.execute(commandStack, this);
	}
	
	@Override
	public void setCommandStack(CommandStack commandStack) {
		this.commandStack = commandStack;
		
		if (commandStack != null) {
			for (Command command : commandStack.getCommands()) {
				String name = command.getName();
				
				if (command.isFailureAbortsExecution()) {
					name = "(!) " + name;
				}
				
				System.out.println("> " + name);
			}
		}
	}
}
