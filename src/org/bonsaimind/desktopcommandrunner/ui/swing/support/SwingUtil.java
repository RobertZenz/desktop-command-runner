/*
 *    Copyright 2024 Robert 'Bobby' Zenz
 * 
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 * 
 *        http://www.apache.org/licenses/LICENSE-2.0
 * 
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.bonsaimind.desktopcommandrunner.ui.swing.support;

import java.lang.reflect.InvocationTargetException;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import javax.swing.SwingUtilities;

import org.bonsaimind.desktopcommandrunner.core.support.ExceptionUtil;
import org.bonsaimind.desktopcommandrunner.core.support.logging.Logger;

public final class SwingUtil {
	private static final Logger LOGGER = new Logger(SwingUtil.class);
	private static Set<String> pendingLaterActionKeys = Collections.synchronizedSet(new HashSet<>());
	
	private SwingUtil() {
	}
	
	public static final void invokeAndWait(Runnable runnable) {
		if (SwingUtilities.isEventDispatchThread()) {
			runnable.run();
			
			return;
		}
		
		try {
			SwingUtilities.invokeAndWait(runnable);
		} catch (InterruptedException | InvocationTargetException e) {
			LOGGER.error("Failed to run action on Swing thread: %s",
					ExceptionUtil.getRootCause(e).getMessage(),
					e);
		}
	}
	
	public static final void invokeLater(Runnable runnable) {
		SwingUtilities.invokeLater(runnable);
	}
	
	public static final void invokeLater(String key, Runnable runnable) {
		if (pendingLaterActionKeys.add(key)) {
			SwingUtilities.invokeLater(() -> {
				try {
					runnable.run();
				} finally {
					pendingLaterActionKeys.remove(key);
				}
			});
		}
	}
}
