/*
 *    Copyright 2024 Robert 'Bobby' Zenz
 * 
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 * 
 *        http://www.apache.org/licenses/LICENSE-2.0
 * 
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.bonsaimind.desktopcommandrunner.ui.swing;

import javax.swing.ImageIcon;
import javax.swing.table.AbstractTableModel;

import org.bonsaimind.desktopcommandrunner.core.Command;
import org.bonsaimind.desktopcommandrunner.core.CommandStack;
import org.bonsaimind.desktopcommandrunner.core.support.ExceptionUtil;
import org.bonsaimind.desktopcommandrunner.ui.swing.resources.Resources;

public class CommandStackTableModel extends AbstractTableModel {
	protected CommandStack commandStack = null;
	
	public CommandStackTableModel(CommandStack commandStack) {
		super();
		
		this.commandStack = commandStack;
	}
	
	@Override
	public Class<?> getColumnClass(int columnIndex) {
		switch (columnIndex) {
			case 0:
				return String.class;
			
			case 1:
				return String.class;
			
			case 2:
				return ImageIcon.class;
			
			case 3:
				return String.class;
			
			case 4:
				return String.class;
			
			default:
				return Object.class;
			
		}
	}
	
	@Override
	public int getColumnCount() {
		return 4;
	}
	
	@Override
	public int getRowCount() {
		return commandStack.getCommands().size();
	}
	
	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		Command command = commandStack.getCommands().get(rowIndex);
		
		switch (columnIndex) {
			case 0:
				if (command.isFailureAbortsExecution()) {
					return "!";
				} else {
					return "";
				}
				
			case 1:
				return command.getName();
			
			case 2:
				switch (command.getCommandState()) {
					case ABORTED:
						return Resources.getImageIcon("package-installed-outdated");
					
					case EXECUTING:
						return Resources.getImageIcon("package-new");
					
					case FAILED_WITH_EXCEPTION:
						return Resources.getImageIcon("package-broken");
					
					case FAILED_WITH_NON_ZERO_EXIT_CODE:
						return Resources.getImageIcon("package-broken");
					
					case PENDING:
						return Resources.getImageIcon("package-available");
					
					case SUCCESS:
						return Resources.getImageIcon("package-installed-updated");
					
					default:
						return null;
					
				}
			case 3:
				return command.getCommandState().name();
			
			case 4:
				if (command.getExecutionResult() != null) {
					if (command.getExecutionResult().getExitCode() != null) {
						return command.getExecutionResult().getExitCode().toString();
					} else {
						Throwable rootCause = ExceptionUtil.getRootCause(command.getExecutionResult().getThrowable());
						
						return rootCause.getClass().getSimpleName() + ": " + rootCause.getMessage();
					}
				} else {
					return "";
				}
				
			default:
				return null;
			
		}
	}
}
