/*
 *    Copyright 2024 Robert 'Bobby' Zenz
 * 
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 * 
 *        http://www.apache.org/licenses/LICENSE-2.0
 * 
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.bonsaimind.desktopcommandrunner.ui.swing.resources;

import java.util.HashMap;
import java.util.Map;

import javax.swing.ImageIcon;

public final class Resources {
	private static Map<String, ImageIcon> imageIconCache = new HashMap<>();
	
	private Resources() {
	}
	
	public static final ImageIcon getImageIcon(String name) {
		ImageIcon imageIcon = imageIconCache.get(name);
		
		if (imageIcon == null) {
			synchronized (imageIconCache) {
				imageIcon = imageIconCache.get(name);
				
				if (imageIcon == null) {
					imageIcon = load(name);
					
					imageIconCache.put(name, imageIcon);
				}
			}
		}
		
		return imageIcon;
	}
	
	private static final ImageIcon load(String name) {
		return new ImageIcon(Resources.class.getResource(name + ".png"));
	}
}
