/*
 *    Copyright 2024 Robert 'Bobby' Zenz
 * 
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 * 
 *        http://www.apache.org/licenses/LICENSE-2.0
 * 
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.bonsaimind.desktopcommandrunner.ui.swing;

import javax.swing.JFrame;

import org.bonsaimind.desktopcommandrunner.core.CommandStack;
import org.bonsaimind.desktopcommandrunner.ui.Ui;
import org.bonsaimind.desktopcommandrunner.ui.swing.support.SwingUtil;

public class SwingUi implements Ui {
	protected MainFrame mainFrame = null;
	
	public SwingUi() {
		super();
		
		SwingUtil.invokeAndWait(() -> {
			mainFrame = new MainFrame();
			mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			mainFrame.setSize(1200, 600);
			mainFrame.setVisible(true);
		});
	}
	
	@Override
	public void execute() {
		SwingUtil.invokeAndWait(() -> {
			mainFrame.execute();
		});
	}
	
	@Override
	public void setCommandStack(CommandStack commandStack) {
		SwingUtil.invokeAndWait(() -> {
			mainFrame.setCommandStack(commandStack);
		});
	}
}
