/*
 *    Copyright 2024 Robert 'Bobby' Zenz
 * 
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 * 
 *        http://www.apache.org/licenses/LICENSE-2.0
 * 
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.bonsaimind.desktopcommandrunner.ui.swing;

import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.PrintWriter;
import java.io.StringWriter;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JToolBar;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.TableModelEvent;
import javax.swing.table.DefaultTableColumnModel;
import javax.swing.table.TableColumn;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.PlainDocument;

import org.bonsaimind.desktopcommandrunner.core.Command;
import org.bonsaimind.desktopcommandrunner.core.CommandStack;
import org.bonsaimind.desktopcommandrunner.core.ExecutionProgressListener;
import org.bonsaimind.desktopcommandrunner.core.Executor;
import org.bonsaimind.desktopcommandrunner.core.process.OutputLine;
import org.bonsaimind.desktopcommandrunner.core.process.OutputLineFormatter;
import org.bonsaimind.desktopcommandrunner.core.support.logging.Logger;
import org.bonsaimind.desktopcommandrunner.ui.swing.resources.Resources;
import org.bonsaimind.desktopcommandrunner.ui.swing.support.SwingUtil;

public class MainFrame extends JFrame implements ExecutionProgressListener {
	private static final Logger LOGGER = new Logger(MainFrame.class);
	
	protected JButton abortButton = null;
	protected CommandStack commandStack = null;
	protected JTable commandStackTable = null;
	protected CommandStackTableModel commandStackTableModel = null;
	protected JTextArea commandTextArea = null;
	protected Executor executor = new Executor();
	protected Document outputDocument = null;
	protected JTextArea outputTextArea = null;
	protected JButton runButton = null;
	protected boolean selectionFollowsExecution = true;
	
	public MainFrame() throws HeadlessException {
		super("Desktop Command Runner");
		
		FontMetrics defaultFontMetrics = getFontMetrics(new JLabel().getFont());
		
		runButton = new JButton();
		runButton.setIcon(Resources.getImageIcon("play"));
		runButton.setText("Run");
		runButton.addActionListener(this::onRunButtonAction);
		
		abortButton = new JButton();
		abortButton.setEnabled(false);
		abortButton.setIcon(Resources.getImageIcon("times"));
		abortButton.setText("Abort");
		abortButton.addActionListener(this::onAbortButtonAction);
		
		JToolBar toolBar = new JToolBar(JToolBar.HORIZONTAL);
		toolBar.setFloatable(false);
		toolBar.add(runButton);
		toolBar.add(abortButton);
		
		TableColumn flagTableColumn = new TableColumn();
		flagTableColumn.setHeaderValue("");
		flagTableColumn.setModelIndex(0);
		flagTableColumn.setPreferredWidth(defaultFontMetrics.getMaxAdvance() * 1);
		
		TableColumn nameTableColumn = new TableColumn();
		nameTableColumn.setHeaderValue("Command");
		nameTableColumn.setModelIndex(1);
		nameTableColumn.setPreferredWidth(defaultFontMetrics.getMaxAdvance() * 10);
		
		TableColumn stateIconTableColumn = new TableColumn();
		stateIconTableColumn.setHeaderValue("");
		stateIconTableColumn.setModelIndex(2);
		stateIconTableColumn.setPreferredWidth(defaultFontMetrics.getMaxAdvance() * 2);
		
		TableColumn stateTableColumn = new TableColumn();
		stateTableColumn.setHeaderValue("State");
		stateTableColumn.setModelIndex(3);
		stateTableColumn.setPreferredWidth(defaultFontMetrics.getMaxAdvance() * 7);
		
		TableColumn resultTableColumn = new TableColumn();
		resultTableColumn.setHeaderValue("Result");
		resultTableColumn.setModelIndex(4);
		resultTableColumn.setPreferredWidth(defaultFontMetrics.getMaxAdvance() * 3);
		
		DefaultTableColumnModel commandStackTableColumnModel = new DefaultTableColumnModel();
		commandStackTableColumnModel.addColumn(flagTableColumn);
		commandStackTableColumnModel.addColumn(nameTableColumn);
		commandStackTableColumnModel.addColumn(stateIconTableColumn);
		commandStackTableColumnModel.addColumn(stateTableColumn);
		commandStackTableColumnModel.addColumn(resultTableColumn);
		
		commandStackTable = new JTable();
		commandStackTable.setAutoCreateColumnsFromModel(false);
		commandStackTable.setColumnModel(commandStackTableColumnModel);
		commandStackTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		commandStackTable.getSelectionModel().addListSelectionListener(this::onCommandStackTableSelectionChanged);
		commandStackTable.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				selectionFollowsExecution = false;
			}
		});
		
		JPanel commandStackPanel = new JPanel();
		commandStackPanel.setBorder(BorderFactory.createTitledBorder("Commands Stack"));
		commandStackPanel.setLayout(new BorderLayout());
		commandStackPanel.add(new JScrollPane(commandStackTable), BorderLayout.CENTER);
		
		commandTextArea = new JTextArea();
		commandTextArea.setEditable(false);
		commandTextArea.setFont(new Font(Font.MONOSPACED, commandTextArea.getFont().getStyle(), commandTextArea.getFont().getSize()));
		commandTextArea.setLineWrap(false);
		commandTextArea.setRows(5);
		
		outputDocument = new PlainDocument();
		
		outputTextArea = new JTextArea();
		outputTextArea.setDocument(outputDocument);
		outputTextArea.setEditable(false);
		outputTextArea.setFont(new Font(Font.MONOSPACED, outputTextArea.getFont().getStyle(), outputTextArea.getFont().getSize()));
		outputTextArea.setLineWrap(false);
		
		JPanel commandPanel = new JPanel();
		commandPanel.setBorder(BorderFactory.createTitledBorder("Command"));
		commandPanel.setLayout(new BorderLayout());
		commandPanel.add(new JScrollPane(commandTextArea), BorderLayout.CENTER);
		
		JPanel outputPanel = new JPanel();
		outputPanel.setBorder(BorderFactory.createTitledBorder("Output"));
		outputPanel.setLayout(new BorderLayout());
		outputPanel.add(new JScrollPane(outputTextArea), BorderLayout.CENTER);
		
		JPanel detailPanel = new JPanel();
		detailPanel.setLayout(new BorderLayout());
		detailPanel.add(commandPanel, BorderLayout.NORTH);
		detailPanel.add(outputPanel, BorderLayout.CENTER);
		
		getContentPane().setLayout(new BorderLayout());
		getContentPane().add(toolBar, BorderLayout.NORTH);
		getContentPane().add(commandStackPanel, BorderLayout.WEST);
		getContentPane().add(detailPanel, BorderLayout.CENTER);
	}
	
	@Override
	public void commandExecutionBegins(Command command) {
		SwingUtil.invokeLater(() -> {
			int commandIndex = commandStack.getCommands().indexOf(command);
			int selectedRowIndex = commandStackTable.getSelectionModel().getLeadSelectionIndex();
			
			if (commandIndex >= 0) {
				LOGGER.debug("Firing row changed event for <commandExecutionBegins> with command <%s>.",
						command.getName());
				
				commandStackTableModel.fireTableChanged(new TableModelEvent(
						commandStackTableModel,
						commandIndex,
						commandIndex,
						TableModelEvent.ALL_COLUMNS,
						TableModelEvent.UPDATE));
				
				if (selectionFollowsExecution) {
					commandStackTable.setRowSelectionInterval(commandIndex, commandIndex);
				} else {
					if (commandIndex == selectedRowIndex) {
						try {
							outputDocument.remove(0, outputDocument.getLength());
						} catch (BadLocationException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				}
			}
		});
		
	}
	
	@Override
	public void commandExecutionFinished(Command command) {
		SwingUtil.invokeLater(() -> {
			int commandIndex = commandStack.getCommands().indexOf(command);
			
			if (commandIndex >= 0) {
				LOGGER.debug("Firing row changed event for <commandExecutionFinished> with command <%s>.",
						command.getName());
				
				commandStackTableModel.fireTableChanged(new TableModelEvent(
						commandStackTableModel,
						commandIndex,
						commandIndex,
						TableModelEvent.ALL_COLUMNS,
						TableModelEvent.UPDATE));
			}
		});
	}
	
	@Override
	public void commandExecutionNewOutput(Command command, OutputLine outputLine) {
		int commandIndex = commandStack.getCommands().indexOf(command);
		int selectedRowIndex = commandStackTable.getSelectionModel().getLeadSelectionIndex();
		
		if (commandIndex == selectedRowIndex) {
			SwingUtil.invokeLater("COMMAND_OUTPUT_REFRESH", () -> {
				LOGGER.debug("Refreshing command output with command <%s>.",
						command.getName());
				
				try {
					outputDocument.insertString(outputDocument.getLength(), OutputLineFormatter.DEFAULT.format(outputLine), null);
				} catch (BadLocationException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				outputTextArea.setCaretPosition(outputTextArea.getText().length());
			});
		}
	}
	
	@Override
	public void commandStackExecutionBegins(CommandStack commandStack) {
		SwingUtil.invokeLater(() -> {
			LOGGER.debug("Firing table changed event for <commandStackExecutionBegins>.");
			
			commandStackTableModel.fireTableChanged(new TableModelEvent(
					commandStackTableModel,
					0,
					commandStackTableModel.getRowCount(),
					TableModelEvent.ALL_COLUMNS,
					TableModelEvent.UPDATE));
		});
	}
	
	@Override
	public void commandStackExecutionFinished(CommandStack commandStack) {
		SwingUtil.invokeLater(() -> {
			LOGGER.debug("Firing table changed event for <commandStackExecutionFinished>.");
			
			commandStackTableModel.fireTableChanged(new TableModelEvent(
					commandStackTableModel,
					0,
					commandStackTableModel.getRowCount(),
					TableModelEvent.ALL_COLUMNS,
					TableModelEvent.UPDATE));
			
			runButton.setEnabled(true);
			abortButton.setEnabled(false);
		});
	}
	
	public MainFrame execute() {
		runButton.setEnabled(false);
		abortButton.setEnabled(true);
		
		selectionFollowsExecution = true;
		
		executor.executeAsynchronous(commandStack, this);
		
		return this;
	}
	
	public MainFrame setCommandStack(CommandStack commandStack) {
		this.commandStack = commandStack;
		
		commandStackTableModel = new CommandStackTableModel(commandStack);
		
		commandStackTable.setModel(commandStackTableModel);
		
		return this;
	}
	
	protected void onAbortButtonAction(ActionEvent actionEvent) {
		executor.abortCurrentExecution();
	}
	
	protected void onCommandStackTableSelectionChanged(ListSelectionEvent listSelectionEvent) {
		updateSelectedCommandView();
	}
	
	protected void onRunButtonAction(ActionEvent actionEvent) {
		execute();
	}
	
	protected void updateSelectedCommandView() {
		int selectedRowIndex = commandStackTable.getSelectionModel().getLeadSelectionIndex();
		
		if (selectedRowIndex >= 0) {
			Command command = commandStack.getCommands().get(selectedRowIndex);
			
			commandTextArea.setText(command.getCommand());
			commandTextArea.setCaretPosition(0);
			
			try {
				outputDocument.remove(0, outputDocument.getLength());
				outputDocument.insertString(0, command.getOutputCollector().getOutputSnapshot(OutputLineFormatter.DEFAULT), null);
			} catch (BadLocationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			if (command.getExecutionResult() != null
					&& command.getExecutionResult().getThrowable() != null) {
				StringWriter exceptionStringWriter = new StringWriter();
				PrintWriter exceptionPrintWriter = new PrintWriter(exceptionStringWriter);
				command.getExecutionResult().getThrowable().printStackTrace(exceptionPrintWriter);
				
				try {
					if (outputDocument.getLength() > 0) {
						outputDocument.insertString(outputDocument.getLength(), "\n\n", null);
					}
					outputDocument.insertString(outputDocument.getLength(), exceptionStringWriter.toString(), null);
				} catch (BadLocationException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
			outputTextArea.setCaretPosition(outputTextArea.getText().length());
		} else {
			commandTextArea.setText("");
			try {
				outputDocument.remove(0, outputDocument.getLength());
			} catch (BadLocationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
}
