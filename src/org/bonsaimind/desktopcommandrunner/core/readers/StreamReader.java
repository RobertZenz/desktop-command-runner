/*
 *    Copyright 2024 Robert 'Bobby' Zenz
 * 
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 * 
 *        http://www.apache.org/licenses/LICENSE-2.0
 * 
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.bonsaimind.desktopcommandrunner.core.readers;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

import org.bonsaimind.desktopcommandrunner.core.CommandStack;
import org.bonsaimind.desktopcommandrunner.core.parsers.CommandStackParser;
import org.bonsaimind.desktopcommandrunner.core.support.logging.Logger;

public class StreamReader {
	private static final Logger LOGGER = new Logger(StreamReader.class);
	
	public StreamReader() {
		super();
	}
	
	public CommandStack readCommandStack(InputStream inputStream, CommandStackParser commandStackParser) throws IOException {
		LOGGER.debug("Reading Command-Stack file from stream.");
		
		BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream, StandardCharsets.UTF_8));
		
		List<String> lines = new ArrayList<>();
		String readLine = null;
		
		while ((readLine = reader.readLine()) != null) {
			lines.add(readLine);
		}
		
		return commandStackParser.parseCommandStack(lines);
	}
}
