/*
 *    Copyright 2024 Robert 'Bobby' Zenz
 * 
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 * 
 *        http://www.apache.org/licenses/LICENSE-2.0
 * 
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.bonsaimind.desktopcommandrunner.core.readers;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;

import org.bonsaimind.desktopcommandrunner.core.CommandStack;
import org.bonsaimind.desktopcommandrunner.core.parsers.CommandStackParser;
import org.bonsaimind.desktopcommandrunner.core.support.logging.Logger;

public class FileReader {
	private static final Logger LOGGER = new Logger(FileReader.class);
	
	public FileReader() {
		super();
	}
	
	public CommandStack readCommandStack(Path filePath, CommandStackParser commandStackParser) throws IOException {
		LOGGER.debug("Reading Command-Stack file from <%s>.",
				filePath);
		
		return commandStackParser.parseCommandStack(Files.readAllLines(filePath, StandardCharsets.UTF_8));
	}
}
