/*
 *    Copyright 2024 Robert 'Bobby' Zenz
 * 
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 * 
 *        http://www.apache.org/licenses/LICENSE-2.0
 * 
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.bonsaimind.desktopcommandrunner.core.support;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.bonsaimind.desktopcommandrunner.core.process.OutputLine;
import org.bonsaimind.desktopcommandrunner.core.process.OutputLineFormatter;

public class OutputCollector {
	protected List<OutputLine> outputLines = new ArrayList<>();
	protected List<OutputLine> outputLinesReadonly = Collections.unmodifiableList(outputLines);
	
	public OutputCollector() {
		super();
	}
	
	public OutputCollector clear() {
		synchronized (outputLines) {
			outputLines.clear();
		}
		
		return this;
	}
	
	public OutputCollector collect(OutputLine outputLine) {
		synchronized (outputLines) {
			outputLines.add(outputLine);
		}
		
		return this;
	}
	
	public String getOutputSnapshot(OutputLineFormatter outputLineFormatter) {
		List<OutputLine> copiedOutputLines = new ArrayList<>();
		
		synchronized (outputLines) {
			copiedOutputLines.addAll(outputLines);
		}
		
		StringBuilder outputBuilder = new StringBuilder();
		
		for (OutputLine outputLine : copiedOutputLines) {
			outputBuilder.append(outputLineFormatter.format(outputLine));
		}
		
		return outputBuilder.toString();
	}
}
