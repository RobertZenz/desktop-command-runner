/*
 *    Copyright 2024 Robert 'Bobby' Zenz
 * 
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 * 
 *        http://www.apache.org/licenses/LICENSE-2.0
 * 
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.bonsaimind.desktopcommandrunner.core.support.logging;

public class Logger {
	private static ClassnameTagLevel globalClassnameTagLevel = ClassnameTagLevel.NONE;
	private static LogLevel globalLevel = LogLevel.INFO;
	private static LevelTagLevel globalLevelTagLevel = LevelTagLevel.NONE;
	private static final String OWN_PACKAGE_PREFIX = "org.bonsaimind.poolofradiancemapextractor.";
	
	protected String fullClassnameTag = null;
	protected java.util.logging.Logger logger = null;
	protected String simpleClassnameTag = null;
	
	public Logger(Class<?> clazz) {
		super();
		
		this.fullClassnameTag = clazz.getName();
		
		if (this.fullClassnameTag.startsWith(OWN_PACKAGE_PREFIX)) {
			this.simpleClassnameTag = this.fullClassnameTag.substring(OWN_PACKAGE_PREFIX.length());
		} else {
			this.simpleClassnameTag = this.fullClassnameTag;
		}
	}
	
	public static final void setGlobalClassnameTagLevel(ClassnameTagLevel classnameTagLevel) {
		Logger.globalClassnameTagLevel = classnameTagLevel;
	}
	
	public static final void setGlobalLevel(LogLevel level) {
		Logger.globalLevel = level;
	}
	
	public static final void setGlobalLevelTagLevel(LevelTagLevel levelTagLevel) {
		Logger.globalLevelTagLevel = levelTagLevel;
	}
	
	public void debug(String message, Object... arguments) {
		print(LogLevel.DEBUG, message, arguments);
	}
	
	public void error(String message, Object... arguments) {
		print(LogLevel.ERROR, message, arguments);
	}
	
	public void info(String message, Object... arguments) {
		print(LogLevel.INFO, message, arguments);
	}
	
	public void warning(String message, Object... arguments) {
		print(LogLevel.WARNING, message, arguments);
	}
	
	protected void print(LogLevel messageLevel, String message, Object... arguments) {
		if (!messageLevel.isEnabled(globalLevel)) {
			return;
		}
		
		String formattedMessage = String.format(message, arguments);
		
		switch (globalClassnameTagLevel) {
			case FULL_CLASSNAME:
				switch (globalLevelTagLevel) {
					case FULL:
						System.out.format("[%s] %s: %s\n",
								messageLevel.tag(),
								fullClassnameTag,
								formattedMessage);
						break;
					
					case NONE:
						System.out.format("%s: %s\n",
								fullClassnameTag,
								formattedMessage);
						break;
					
				}
				break;
			
			case NONE:
				switch (globalLevelTagLevel) {
					case FULL:
						System.out.format("[%s] %s\n",
								messageLevel.tag(),
								formattedMessage);
						break;
					
					case NONE:
						System.out.format("%s\n",
								formattedMessage);
						break;
					
				}
				break;
			
			case SIMPLE_CLASSNAME:
				switch (globalLevelTagLevel) {
					case FULL:
						System.out.format("[%s] %s: %s\n",
								messageLevel.tag(),
								simpleClassnameTag,
								formattedMessage);
						break;
					
					case NONE:
						System.out.format("%s: %s\n",
								simpleClassnameTag,
								formattedMessage);
						break;
					
				}
				break;
		}
		
		if (arguments != null
				&& arguments.length > 0
				&& arguments[arguments.length - 1] instanceof Throwable) {
			Throwable throwable = (Throwable)arguments[arguments.length - 1];
			
			throwable.printStackTrace(System.out);
		}
	}
}
