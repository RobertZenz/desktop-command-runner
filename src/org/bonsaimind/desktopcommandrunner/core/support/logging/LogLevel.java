/*
 *    Copyright 2024 Robert 'Bobby' Zenz
 * 
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 * 
 *        http://www.apache.org/licenses/LICENSE-2.0
 * 
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.bonsaimind.desktopcommandrunner.core.support.logging;

public enum LogLevel {
	DEBUG("DEBUG", 4), ERROR("ERROR", 1), INFO("INFO ", 3), NONE("NONE", 0), WARNING("WARN ", 2);
	
	private final int stage;
	private final String tag;
	
	private LogLevel(String tag, int stage) {
		this.tag = tag;
		this.stage = stage;
	}
	
	public final boolean isEnabled(LogLevel requiredLogLevel) {
		return stage <= requiredLogLevel.stage;
	}
	
	public final int stage() {
		return stage;
	}
	
	public final String tag() {
		return tag;
	}
}
