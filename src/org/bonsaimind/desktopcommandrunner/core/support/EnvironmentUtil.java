/*
 *    Copyright 2024 Robert 'Bobby' Zenz
 * 
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 * 
 *        http://www.apache.org/licenses/LICENSE-2.0
 * 
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.bonsaimind.desktopcommandrunner.core.support;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class EnvironmentUtil {
	private static final Pattern ENVIRONMENT_VARIABLE_PATTERN = Pattern.compile("\\$("
			+ "(?<NAME>[A-Za-z0-9_]+)"
			+ "|\\{(?<NAME2>[^}]+)}"
			+ ")");
	
	private EnvironmentUtil() {
	}
	
	public static final List<String> resolveEnvironmentVariables(List<String> parameters) {
		List<String> resolvedParameters = new ArrayList<>(parameters.size());
		
		for (String parameter : parameters) {
			resolvedParameters.add(resolveEnvironmentVariables(parameter));
		}
		
		return resolvedParameters;
	}
	
	public static final String resolveEnvironmentVariables(String parameter) {
		StringBuffer resolvedParameterBuilder = new StringBuffer();
		
		Matcher variablesMatcher = ENVIRONMENT_VARIABLE_PATTERN.matcher(parameter);
		
		while (variablesMatcher.find()) {
			String variableName = variablesMatcher.group("NAME");
			
			if (variableName == null) {
				variableName = variablesMatcher.group("NAME2");
			}
			
			String variableValue = System.getenv(variableName);
			
			if (variableValue != null) {
				variablesMatcher.appendReplacement(resolvedParameterBuilder, variableValue);
			}
		}
		
		variablesMatcher.appendTail(resolvedParameterBuilder);
		
		return resolvedParameterBuilder.toString();
	}
}
