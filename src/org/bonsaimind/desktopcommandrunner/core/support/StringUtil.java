/*
 *    Copyright 2024 Robert 'Bobby' Zenz
 * 
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 * 
 *        http://www.apache.org/licenses/LICENSE-2.0
 * 
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.bonsaimind.desktopcommandrunner.core.support;

import java.util.LinkedList;
import java.util.List;
import java.util.StringTokenizer;

public final class StringUtil {
	private StringUtil() {
	}
	
	public static final List<String> convertToCommandLine(String command) {
		List<String> commandLine = new LinkedList<>();
		
		StringTokenizer commandTokenizer = new StringTokenizer(command, " \t'\"\n\r", true);
		
		StringBuilder currentPartBuilder = new StringBuilder();
		
		boolean insideDoubleQuotes = false;
		boolean insideSingleQuotes = false;
		
		while (commandTokenizer.hasMoreTokens()) {
			String token = commandTokenizer.nextToken();
			
			switch (token) {
				case " ":
				case "\t":
					if (!insideDoubleQuotes && !insideSingleQuotes) {
						if (currentPartBuilder.length() > 0) {
							commandLine.add(currentPartBuilder.toString());
							currentPartBuilder.delete(0, currentPartBuilder.length());
						}
					} else {
						currentPartBuilder.append(token);
					}
					break;
				
				case "'":
					if (!insideDoubleQuotes) {
						insideSingleQuotes = !insideSingleQuotes;
					} else {
						currentPartBuilder.append(token);
					}
					break;
				
				case "\"":
					if (!insideSingleQuotes) {
						insideDoubleQuotes = !insideDoubleQuotes;
					} else {
						currentPartBuilder.append(token);
					}
					break;
				
				case "\n":
				case "\r":
					if (!insideDoubleQuotes && !insideSingleQuotes) {
						if (currentPartBuilder.length() > 0) {
							commandLine.add(currentPartBuilder.toString());
							currentPartBuilder.delete(0, currentPartBuilder.length());
						}
					} else {
						currentPartBuilder.append(token);
					}
					break;
				
				default:
					currentPartBuilder.append(token);
					break;
			}
		}
		
		if (currentPartBuilder.length() > 0) {
			commandLine.add(currentPartBuilder.toString());
		}
		
		return commandLine;
	}
	
	public static final String padLeftRight(String text, String padding, int paddingWidth) {
		StringBuilder commandHeaderBuilder = new StringBuilder();
		commandHeaderBuilder.append("\n");
		
		int leadCount = (paddingWidth / 2) - (text.length() / 2) - 1;
		
		for (int counter = 0; counter < leadCount; counter++) {
			commandHeaderBuilder.append("=");
		}
		
		commandHeaderBuilder.append(" ");
		commandHeaderBuilder.append(text);
		commandHeaderBuilder.append(" ");
		
		int trailCount = (paddingWidth / 2) - (int)Math.ceil(text.length() / 2.0d) - 1;
		
		for (int counter = 0; counter < trailCount; counter++) {
			commandHeaderBuilder.append("=");
		}
		
		return commandHeaderBuilder.toString();
	}
}
