/*
 *    Copyright 2024 Robert 'Bobby' Zenz
 * 
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 * 
 *        http://www.apache.org/licenses/LICENSE-2.0
 * 
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.bonsaimind.desktopcommandrunner.core;

import java.io.IOException;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.TimeUnit;

import org.bonsaimind.desktopcommandrunner.core.process.OutputSource;
import org.bonsaimind.desktopcommandrunner.core.process.ProcessOutputReaderThread;
import org.bonsaimind.desktopcommandrunner.core.support.EnvironmentUtil;
import org.bonsaimind.desktopcommandrunner.core.support.StringUtil;
import org.bonsaimind.desktopcommandrunner.core.support.logging.Logger;

public class Executor {
	private static final Logger LOGGER = new Logger(Executor.class);
	
	protected volatile boolean abort = false;
	protected Queue<Process> executingProcesses = new ConcurrentLinkedQueue<>();
	
	public Executor() {
		super();
		
		Runtime.getRuntime().addShutdownHook(new Thread(this::onJvmShutdown));
	}
	
	public Executor abortCurrentExecution() {
		abort = true;
		
		return this;
	}
	
	public synchronized Executor execute(CommandStack commandStack, ExecutionProgressListener executionProgressConsumer) {
		abort = false;
		
		for (Command command : commandStack.getCommands()) {
			command.setCommandState(CommandState.PENDING);
			command.setExecutionResult(null);
			command.getOutputCollector().clear();
		}
		
		if (executionProgressConsumer != null) {
			executionProgressConsumer.commandStackExecutionBegins(commandStack);
		}
		
		for (Command command : commandStack.getCommands()) {
			if (!abort) {
				execute(command, executionProgressConsumer);
			} else {
				command.setCommandState(CommandState.ABORTED);
			}
		}
		
		if (executionProgressConsumer != null) {
			executionProgressConsumer.commandStackExecutionFinished(commandStack);
		}
		
		return this;
	}
	
	public synchronized Executor executeAsynchronous(CommandStack commandStack, ExecutionProgressListener executionProgressConsumer) {
		abort = false;
		
		Thread thread = new Thread(() -> {
			execute(commandStack, executionProgressConsumer);
		});
		
		thread.start();
		
		return this;
	}
	
	protected Executor execute(Command command, ExecutionProgressListener executionProgressConsumer) {
		if (executionProgressConsumer != null) {
			executionProgressConsumer.commandExecutionBegins(command);
		}
		
		List<String> commandLine = StringUtil.convertToCommandLine(command.getCommand());
		commandLine = EnvironmentUtil.resolveEnvironmentVariables(commandLine);
		
		ProcessBuilder processBuilder = new ProcessBuilder(commandLine);
		
		try {
			LOGGER.debug("Starting process for <%s>.",
					command.getName());
			
			Process process = processBuilder.start();
			
			executingProcesses.add(process);
			
			command.getOutputCollector().clear();
			command.setCommandState(CommandState.EXECUTING);
			
			Thread standardOutProcessOutputReaderThread = new ProcessOutputReaderThread(
					process,
					OutputSource.STANDARD_OUT,
					command.getOutputCollector(),
					command,
					executionProgressConsumer);
			standardOutProcessOutputReaderThread.setDaemon(true);
			standardOutProcessOutputReaderThread.start();
			
			Thread standardErrorProcessOutputReaderThread = new ProcessOutputReaderThread(
					process,
					OutputSource.STANDARD_ERROR,
					command.getOutputCollector(),
					command,
					executionProgressConsumer);
			standardErrorProcessOutputReaderThread.setDaemon(true);
			standardErrorProcessOutputReaderThread.start();
			
			while (!abort && !process.waitFor(25l, TimeUnit.MILLISECONDS)) {
				// Nothing to do, keep looping.
			}
			
			if (abort) {
				LOGGER.debug("Abort requested, killing process for <%s>.",
						command.getName());
				
				killProcess(process);
				
				executingProcesses.remove(process);
				
				command.setCommandState(CommandState.ABORTED);
				
				if (executionProgressConsumer != null) {
					executionProgressConsumer.commandExecutionFinished(command);
				}
				
				return this;
			}
			
			if (process.exitValue() == 0) {
				standardOutProcessOutputReaderThread.join();
				standardErrorProcessOutputReaderThread.join();
				
				command.setCommandState(CommandState.SUCCESS);
				command.setExecutionResult(ExecutionResult.success());
			} else {
				standardOutProcessOutputReaderThread.join();
				standardErrorProcessOutputReaderThread.join();
				
				command.setCommandState(CommandState.FAILED_WITH_NON_ZERO_EXIT_CODE);
				command.setExecutionResult(ExecutionResult.failure(process.exitValue()));
				
				if (command.isFailureAbortsExecution()) {
					abortCurrentExecution();
				}
			}
			
			executingProcesses.remove(process);
		} catch (InterruptedException | IOException e) {
			command.setCommandState(CommandState.FAILED_WITH_EXCEPTION);
			command.setExecutionResult(ExecutionResult.failure(e));
			
			if (command.isFailureAbortsExecution()) {
				abortCurrentExecution();
			}
		}
		
		if (executionProgressConsumer != null) {
			executionProgressConsumer.commandExecutionFinished(command);
		}
		
		return this;
	}
	
	protected void killProcess(Process process) throws InterruptedException {
		if (process.isAlive()) {
			LOGGER.debug("To be killed process still alive, destroying.");
			
			process.destroy();
			
			if (!process.waitFor(250l, TimeUnit.MILLISECONDS)) {
				LOGGER.debug("To be killed process still alive, forcibly destroying.");
				
				process.destroyForcibly();
			}
		}
	}
	
	protected void onJvmShutdown() {
		LOGGER.debug("JVM shutdown in progress, killing running child processes (if any).");
		
		abortCurrentExecution();
		
		Process executingProcess = null;
		
		while ((executingProcess = executingProcesses.poll()) != null) {
			try {
				killProcess(executingProcess);
			} catch (InterruptedException e) {
				// Ignore the exception.
			}
		}
		
		LOGGER.debug("Executor shutdown completed.");
	}
}
