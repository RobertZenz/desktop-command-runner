/*
 *    Copyright 2024 Robert 'Bobby' Zenz
 * 
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 * 
 *        http://www.apache.org/licenses/LICENSE-2.0
 * 
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.bonsaimind.desktopcommandrunner.core;

import org.bonsaimind.desktopcommandrunner.core.support.OutputCollector;

public class Command {
	protected String command = null;
	protected CommandState commandState = CommandState.PENDING;
	protected ExecutionResult executionResult = null;
	protected boolean failureAbortsExecution = false;
	protected String name = null;
	protected OutputCollector outputCollector = new OutputCollector();
	
	public Command(String name, String command, boolean failureAbortsExecution) {
		super();
		
		this.name = name;
		this.command = command;
		this.failureAbortsExecution = failureAbortsExecution;
	}
	
	public String getCommand() {
		return command;
	}
	
	public CommandState getCommandState() {
		return commandState;
	}
	
	public ExecutionResult getExecutionResult() {
		return executionResult;
	}
	
	public String getName() {
		return name;
	}
	
	public OutputCollector getOutputCollector() {
		return outputCollector;
	}
	
	public boolean isFailureAbortsExecution() {
		return failureAbortsExecution;
	}
	
	public void setCommandState(CommandState commandState) {
		this.commandState = commandState;
	}
	
	public Command setExecutionResult(ExecutionResult executionResult) {
		this.executionResult = executionResult;
		
		return this;
	}
}
