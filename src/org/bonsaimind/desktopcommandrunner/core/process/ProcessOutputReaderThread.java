/*
 *    Copyright 2024 Robert 'Bobby' Zenz
 * 
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 * 
 *        http://www.apache.org/licenses/LICENSE-2.0
 * 
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.bonsaimind.desktopcommandrunner.core.process;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.time.OffsetDateTime;

import org.bonsaimind.desktopcommandrunner.core.Command;
import org.bonsaimind.desktopcommandrunner.core.ExecutionProgressListener;
import org.bonsaimind.desktopcommandrunner.core.support.OutputCollector;

public class ProcessOutputReaderThread extends Thread {
	protected Command command = null;
	protected ExecutionProgressListener executionProgressConsumer = null;
	protected OutputCollector outputCollector = null;
	protected Process process = null;
	protected OutputSource processOutputSource = null;
	
	public ProcessOutputReaderThread(
			Process process,
			OutputSource processOutputSource,
			OutputCollector outputCollector,
			Command command,
			ExecutionProgressListener executionProgressConsumer) {
		super();
		
		this.process = process;
		this.processOutputSource = processOutputSource;
		this.outputCollector = outputCollector;
		this.command = command;
		this.executionProgressConsumer = executionProgressConsumer;
	}
	
	@Override
	public void run() {
		try {
			InputStream inputStream = null;
			
			switch (processOutputSource) {
				case STANDARD_ERROR:
					inputStream = process.getErrorStream();
					break;
				
				case STANDARD_OUT:
				default:
					inputStream = process.getInputStream();
					break;
				
			}
			
			BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream, StandardCharsets.UTF_8));
			
			String readLine = null;
			
			while ((readLine = reader.readLine()) != null) {
				OutputLine outputLine = new OutputLine(
						OffsetDateTime.now(),
						processOutputSource,
						readLine);
				
				outputCollector.collect(outputLine);
				
				if (executionProgressConsumer != null) {
					executionProgressConsumer.commandExecutionNewOutput(command, outputLine);
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
