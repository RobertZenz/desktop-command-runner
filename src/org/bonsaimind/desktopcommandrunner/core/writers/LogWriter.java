/*
 *    Copyright 2024 Robert 'Bobby' Zenz
 * 
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 * 
 *        http://www.apache.org/licenses/LICENSE-2.0
 * 
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.bonsaimind.desktopcommandrunner.core.writers;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;

import org.bonsaimind.desktopcommandrunner.core.Command;
import org.bonsaimind.desktopcommandrunner.core.CommandStack;
import org.bonsaimind.desktopcommandrunner.core.process.OutputLineFormatter;
import org.bonsaimind.desktopcommandrunner.core.support.ExceptionUtil;
import org.bonsaimind.desktopcommandrunner.core.support.StringUtil;

public class LogWriter {
	public LogWriter() {
		super();
	}
	
	public LogWriter write(CommandStack commandStack, Path file) throws IOException {
		try (BufferedWriter writer = Files.newBufferedWriter(file, StandardCharsets.UTF_8)) {
			writeOverview(commandStack, writer);
			writeExecutions(commandStack, writer);
			writeSummary(commandStack, writer);
		}
		
		return this;
	}
	
	protected void writeExecutions(CommandStack commandStack, BufferedWriter writer) throws IOException {
		for (Command command : commandStack.getCommands()) {
			String name = command.getName();
			
			if (command.isFailureAbortsExecution()) {
				name = "(!) " + name;
			}
			
			writer.write(StringUtil.padLeftRight(name, "=", 80) + "\n");
			writer.write(command.getOutputCollector().getOutputSnapshot(OutputLineFormatter.DEFAULT));
			
			if (command.getExecutionResult() != null) {
				if (command.getExecutionResult().wasSuccessful()) {
					System.out.format("EXIT: 0\n");
				} else if (command.getExecutionResult().getExitCode() != null) {
					System.out.format("EXIT: %d\n",
							command.getExecutionResult().getExitCode());
				} else if (command.getExecutionResult().getThrowable() != null) {
					Throwable rootCause = ExceptionUtil.getRootCause(command.getExecutionResult().getThrowable());
					
					System.out.format("EXCEPTION: %s: %s\n",
							rootCause.getClass().getSimpleName(),
							rootCause.getMessage());
					command.getExecutionResult().getThrowable().printStackTrace(System.out);
				}
			}
		}
	}
	
	protected void writeOverview(CommandStack commandStack, BufferedWriter writer) throws IOException {
		for (Command command : commandStack.getCommands()) {
			String name = command.getName();
			
			if (command.isFailureAbortsExecution()) {
				name = "(!) " + name;
			}
			
			writer.write("> " + name + "\n");
		}
	}
	
	protected void writeSummary(CommandStack commandStack, BufferedWriter writer) throws IOException {
		for (Command command : commandStack.getCommands()) {
			String abortMarker = "";
			
			if (command.isFailureAbortsExecution()) {
				abortMarker = "(!) ";
			}
			
			if (command.getExecutionResult() != null) {
				if (command.getExecutionResult().getThrowable() != null) {
					writer.write(String.format("<[ERR] %s%s\n",
							abortMarker,
							command.getName()));
				} else {
					writer.write(String.format("<[%3d] %s%s\n",
							command.getExecutionResult().getExitCode(),
							abortMarker,
							command.getName()));
				}
			} else {
				writer.write(String.format("<[   ] %s%s\n",
						abortMarker,
						command.getName()));
			}
		}
	}
}
