/*
 *    Copyright 2024 Robert 'Bobby' Zenz
 * 
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 * 
 *        http://www.apache.org/licenses/LICENSE-2.0
 * 
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.bonsaimind.desktopcommandrunner.core;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class CommandStack {
	protected List<Command> commands = new ArrayList<>();
	protected List<Command> commandsReadonly = Collections.unmodifiableList(commands);
	
	public CommandStack() {
		super();
	}
	
	public CommandStack addCommand(Command command) {
		commands.add(command);
		
		return this;
	}
	
	public CommandStack addCommandStack(CommandStack commandStack) {
		for (Command command : commandStack.getCommands()) {
			addCommand(command);
		}
		
		return this;
	}
	
	public List<Command> getCommands() {
		return commandsReadonly;
	}
}
