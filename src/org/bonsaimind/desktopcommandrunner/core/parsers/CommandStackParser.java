/*
 *    Copyright 2024 Robert 'Bobby' Zenz
 * 
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 * 
 *        http://www.apache.org/licenses/LICENSE-2.0
 * 
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.bonsaimind.desktopcommandrunner.core.parsers;

import java.util.Iterator;
import java.util.List;

import org.bonsaimind.desktopcommandrunner.core.Command;
import org.bonsaimind.desktopcommandrunner.core.CommandStack;

public class CommandStackParser {
	public CommandStackParser() {
		super();
	}
	
	public CommandStack parseCommandStack(List<String> commandStackLines) {
		trimFileStart(commandStackLines);
		
		CommandStack commandStack = new CommandStack();
		
		String commandName = "";
		boolean commandFailureAbortsExecution = false;
		StringBuilder commandBuilder = new StringBuilder();
		
		for (String line : commandStackLines) {
			if ((line.startsWith("[") || line.startsWith("!["))
					&& line.endsWith("]")) {
				if (commandBuilder.length() > 0) {
					if (commandName.isEmpty()) {
						commandName = commandBuilder.toString().replaceAll("[ \t\n\r]+", " ").trim();
					}
					
					commandStack.addCommand(new Command(
							commandName,
							commandBuilder.toString().trim(),
							commandFailureAbortsExecution));
					commandBuilder.delete(0, commandBuilder.length());
				}
				
				if (line.startsWith("![")) {
					commandName = line.substring(2, line.length() - 1);
					commandFailureAbortsExecution = true;
				} else {
					commandName = line.substring(1, line.length() - 1);
					commandFailureAbortsExecution = false;
				}
			} else {
				commandBuilder
						.append(line)
						.append("\n");
			}
		}
		
		if (commandBuilder.length() > 0) {
			if (commandName.isEmpty()) {
				commandName = commandBuilder.toString().replaceAll("[ \t\n\r]+", " ");
			}
			
			commandStack.addCommand(new Command(
					commandName,
					commandBuilder.toString().trim(),
					commandFailureAbortsExecution));
		}
		
		return commandStack;
	}
	
	protected void trimFileStart(List<String> lines) {
		if (lines.isEmpty()) {
			return;
		}
		
		Iterator<String> linesIterator = lines.iterator();
		
		String firstLine = linesIterator.next();
		
		if (firstLine.startsWith("#!")) {
			linesIterator.remove();
		} else if (firstLine.trim().isEmpty()) {
			linesIterator.remove();
		} else {
			return;
		}
		
		while (linesIterator.hasNext()) {
			String line = linesIterator.next();
			
			if (line.trim().isEmpty()) {
				linesIterator.remove();
			} else {
				return;
			}
		}
	}
}
