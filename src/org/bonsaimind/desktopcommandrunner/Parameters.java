/*
 *    Copyright 2024 Robert 'Bobby' Zenz
 * 
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 * 
 *        http://www.apache.org/licenses/LICENSE-2.0
 * 
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.bonsaimind.desktopcommandrunner;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.LinkedList;
import java.util.List;

import org.bonsaimind.desktopcommandrunner.core.support.logging.ClassnameTagLevel;
import org.bonsaimind.desktopcommandrunner.core.support.logging.LevelTagLevel;
import org.bonsaimind.desktopcommandrunner.core.support.logging.LogLevel;

public class Parameters {
	protected ClassnameTagLevel classnameTagLevel = ClassnameTagLevel.NONE;
	protected boolean execute = true;
	protected List<Path> filesToLoad = new LinkedList<>();
	protected boolean help = false;
	protected LevelTagLevel levelTagLevel = LevelTagLevel.NONE;
	protected LogLevel logLevel = LogLevel.INFO;
	protected String ui = null;
	
	public Parameters(String[] arguments) {
		super();
		
		if (arguments != null && arguments.length > 0) {
			for (String argument : arguments) {
				if (argument.equals("--cli")) {
					ui = "CommandLine";
				} else if (argument.equals("--command-line")) {
					ui = "CommandLine";
				} else if (argument.startsWith("--debug")) {
					classnameTagLevel = ClassnameTagLevel.SIMPLE_CLASSNAME;
					levelTagLevel = LevelTagLevel.FULL;
					logLevel = LogLevel.DEBUG;
				} else if (argument.startsWith("--errors")) {
					classnameTagLevel = ClassnameTagLevel.NONE;
					levelTagLevel = LevelTagLevel.NONE;
					logLevel = LogLevel.ERROR;
				} else if (argument.equals("--execute")) {
					execute = true;
				} else if (argument.equals("--gui")) {
					ui = "Swing";
				} else if (argument.equals("-h")
						|| argument.equals("--help")) {
					help = true;
				} else if (argument.startsWith("--log-classname-tag-level=")) {
					classnameTagLevel = ClassnameTagLevel.valueOf(argument.substring("--log-classname-tag-level=".length()));
				} else if (argument.startsWith("--log-level-tag-level=")) {
					levelTagLevel = LevelTagLevel.valueOf(argument.substring("--log-level-tag-level=".length()));
				} else if (argument.startsWith("--log-level=")) {
					logLevel = LogLevel.valueOf(argument.substring("--log-level=".length()));
				} else if (argument.equals("--no-execute")) {
					execute = false;
				} else if (argument.startsWith("--quiet")) {
					classnameTagLevel = ClassnameTagLevel.NONE;
					levelTagLevel = LevelTagLevel.NONE;
					logLevel = LogLevel.NONE;
				} else if (argument.equals("--swing")) {
					ui = "Swing";
				} else if (argument.startsWith("--ui=")) {
					ui = argument.substring("--ui=".length());
				} else if (argument.startsWith("--verbose")) {
					classnameTagLevel = ClassnameTagLevel.NONE;
					levelTagLevel = LevelTagLevel.NONE;
					logLevel = LogLevel.DEBUG;
				} else if (argument.startsWith("-") && !argument.equals("-")) {
					help = true;
				} else {
					filesToLoad.add(Paths.get(argument));
				}
			}
		}
	}
	
	public ClassnameTagLevel getClassnameTagLevel() {
		return classnameTagLevel;
	}
	
	public boolean getExecute() {
		return execute;
	}
	
	public List<Path> getFilesToLoad() {
		return filesToLoad;
	}
	
	public boolean getHelp() {
		return help;
	}
	
	public LevelTagLevel getLevelTagLevel() {
		return levelTagLevel;
	}
	
	public LogLevel getLogLevel() {
		return logLevel;
	}
	
	public String getUi() {
		return ui;
	}
}
