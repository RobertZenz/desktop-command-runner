Desktop Command Runner
======================

This is a proof-of-concept/prototype.

This program allows you execute commands in order with a (halfway) acceptable UI which gives you a good overview of what commands have already been executed, which are still pending and what the result and output of each command was.

![A screenshot of the main (Swing) window showing the execution of the "test.commands" file.](./examples/screenshot-test.png)

The program accepts one or more "Command Stack" files which contain the commands which should be executed.


Command Stacks
--------------

Command-Stack files must be in the following format:

    [COMMAND_NAME]
    COMMAND
    
    [COMMAND_NAME2]
    COMMAND2

The name of a command is specified in square brackets on its own line, that line
must start and end with a square bracket. Anything between the first and
the last bracket will be used as name, that can also include other square
brackets.

Everything following the command name line will be the command itself.
Newlines and whitespace are insignificant unless in quotes (single or double)
and will be collapsed to a single space when executing the command. Leading and
trailing whitespace will also be trimmed.

    [Whitespace example]
    rm
        --force
        --recursive
        that.file

Will be collapsed into "rm --force --recursive that.file", so a command can be
split over multiple lines for readability without the need to escape the
newlines. To preserve whitespace single or double quotes can be used:

    [Whitespace preservation]
    echo "First Line
    Second Line
    Third Line"

While result in three lines being echoed.

By default, the exit status of a command is ignored, if the stack should stop
being executed if a command fails, the command name can be prefixed with a "!":

    ![Stop when fail]
    command that might fail

Executing "complex" commands is not supported, however, nothing stops you from
using a shell:

    [Shell commands]
    sh -c '
        if [ -f "some.file" ]; then
            rm "some.file"
        else
            touch "that-other.file"
        fi
    '

Or, more ideally, you create a "library" of functions in a shell script which
can then be invoked as needed.

    [Shell commands2]
    ./actions.sh that-action
    
    [Shell commands3]
    ./actions.sh that-other-action

Variables in the command with the format "$VARIABLE" and "${VARIABLE}" will be
resolved from the environment if they are available. For example:

    [Variable]
    ls -l "$HOME"

Will be resolved to "ls -l /home/user/" from the environment of the current
(Java) process. If the variable is not available in the environment, it will
not be replaced.

Shebang lines are supported at the beginning of a Command-Stack file:

    #!/home/user/.local/bin/desktop-command-runner
    
    [First Command]
    mount

They will simply be ignored when loading/reading the file.
